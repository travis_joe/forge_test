package com.example.examplemod;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = ExampleMod.MOD_ID, version = ExampleMod.VERSION)
public class ExampleMod
{
    public static final String MOD_ID = "examplemod";
    public static final String VERSION = "1.0";
    private ExampleBlock block;
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        block = new ExampleBlock();
        GameRegistry.registerBlock(block, "exampleBlock");
        MinecraftForge.EVENT_BUS.register(new EntityListener(this));

        Item exampleBlockItem = GameRegistry.findItem("examplemod", "exampleBlock");
        ModelResourceLocation exampleBlockModel = new ModelResourceLocation("examplemod:exampleBlock", "inventory");
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(exampleBlockItem, 0, exampleBlockModel);


    }

    public ExampleBlock getExampleBlock() {
        return block;
    }
}
