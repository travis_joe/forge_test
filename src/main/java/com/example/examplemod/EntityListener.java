package com.example.examplemod;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 * Created by travis on 5/17/16.
 */
public class EntityListener {
    private ExampleMod mod;

    public EntityListener(ExampleMod mod) {
        this.mod = mod;
    }
    @SubscribeEvent
    public void onBlockBreak(BlockEvent.BreakEvent event) {
        /*event.setCanceled(true);
        if(event.getState().getBlock().equals(Blocks.diamond_block)) {
            event.getWorld().setBlockState(event.getPos(), Blocks.coal_block.getDefaultState());
            return;
        }
        event.getWorld().setBlockState(event.getPos(), Blocks.diamond_block.getDefaultState());*/

    }

    @SubscribeEvent
    public void onEntityDamage(AttackEntityEvent event) {
        event.getTarget().setPosition(event.getTarget().posX, event.getTarget().posY+10, event.getTarget().posZ);
        event.getTarget().velocityChanged = true;
        event.getEntityPlayer().addChatMessage(new TextComponentString("You hit " + event.getTarget().getName()));
    }

    @SubscribeEvent
    public void onEntityFall(LivingFallEvent event) {
        if(event.getEntityLiving() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.getEntityLiving();
            if (!player.getEntityWorld().isRemote) {
                int blockX = player.getPosition().getX();
                int blockY = player.getPosition().getY() - 1;
                int blockZ = player.getPosition().getZ();

                if(player.getEntityWorld().getBlockState(new BlockPos(blockX, blockY, blockZ)).getBlock() instanceof ExampleBlock) {
                    player.motionY = 1;
                    player.velocityChanged = true;
                }
            }
        }
    }
}
