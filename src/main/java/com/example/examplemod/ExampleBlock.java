package com.example.examplemod;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

/**
 * Created by travis on 5/20/16.
 */
public class ExampleBlock extends Block {

    public ExampleBlock() {
        super(Material.iron);
        setUnlocalizedName("exampleBlock");
        setCreativeTab(CreativeTabs.tabBlock);
        setResistance(5.0f);
        setHardness(10.0f);
        setLightLevel(1.0f);
    }

}
